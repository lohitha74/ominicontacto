#########################################################
### DevEnv. Omnileads Docker container for developers ###
#########################################################

For any question or contribution you want to do in these environment don't hesitate you open an issue in the Gitlab's project:

https://gitlab.com/omnileads/ominicontacto/issues

Thank you for your cooperation!
