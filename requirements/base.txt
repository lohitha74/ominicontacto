# base requirements
cairocffi==0.8.0
CairoSVG==1.0.22
cffi==1.9.1
coverage==4.4.1
cryptography==2.4.2
cssselect==1.0.1
decorator==4.0.10
Django==1.9.7
django-compressor==2.2
django-constance==2.3.1
django-cors-headers==2.4.0
django-crispy-forms==1.6.0
django-defender==0.5.5
django-formtools==2.1
django-js-reverse==0.8.2
django-simple-history==1.9.1
django-widget-tweaks==1.4.1
djangorestframework==3.6.2
enum34==1.1.6
fabric==2.4.0
factory-boy==2.9.2
ipython==5.1.0
ipython-genutils==0.1.0
lxml==3.6.4
olefile==0.44
paramiko==2.4.2
pathlib2==2.1.0
pexpect==4.2.1
pickleshare==0.7.4
Pillow==4.0.0
prompt-toolkit==1.0.7
psycopg2==2.7.5
ptyprocess==0.5.1
py2-ipaddress==3.4.1
pycha==0.7.0
pycparser==2.17
pygal==2.3.0
-e git+https://github.com/alejandrozf/pyst2@issue-46#egg=pyst2
python-crontab==2.2.7
Pygments==2.1.3
reportlab==3.3.0
requests==2.12.5
simplegeneric==0.8.1
six==1.10.0
tinycss==0.4
traitlets==4.3.1
unicodecsv==0.14.1
wcwidth==0.1.7
